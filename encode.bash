#!/usr/bin/env bash
[[ ${BASH_SOURCE:0:1} = '.' ]] && encoderExecDir="$PWD" || { cd ${BASH_SOURCE%/*} ; encoderExecDir="$PWD" ; cd - &> /dev/null ; }

source ${encoderExecDir}/encode_user_variables.bash

[[ -z $cdromDevice ]] && { readarray -t scanbus <<< $(cdrecord -scanbus)
for scanDevice in "${!scanbus[@]}" ; do
[[ ${scanbus[scanDevice]} =~ 'CD-ROM' ]] && cdromDevice="${scanbus[scanDevice]:1:5}" && break
done ; }

[[ -z $devDevice ]] && { devDevice=$(find /dev -group cdrom -maxdepth 1 -print -quit) ; }

iconvConvert () { if [[ $1 = *.inf ]]; then readarray -t loadINF <<< $(iconv -f iso8859-1 -t utf-8 $1)
elif [[ $1 = *.cddb ]]; then readarray -t cddbFile <<< $(iconv -f iso8859-1 -t utf-8 $1)
fi ; }

search () { for input in "${!loadINF[@]}" "${!cddbFile[@]}" ; do
[[ ${loadINF[input]} =~ $1 ]] && { output="${loadINF[input]}" ; break ; }
[[ ${cddbFile[input]} =~ $1 ]] && { output="${cddbFile[input]}" ; break ; }
done ; }

encode () { SECONDS='0' ; trap 'return' INT ; set +m
local workingDir="${workingDir:-/dev/shm}"
local extractDir="${extractDir:-${workingDir}/audio_CD}"
local errorChecking="${errorChecking:--paranoia paraopts=minoverlap=30,readahead=3200,retries=50}"
local twoPlaces='00'
local {trackList,cddbFile,loadINF,output,Albumperformer,Tracktitle,Albumtitle,DGENRE,DYEAR,number,writeOnce,wavComplete}

for option in "$@" ; do case $option in
help) source ${encoderExecDir}/encode_help.bash ; return ;;
checkoff|wavcheckoff) unset errorChecking ; echo -e ' \xe2\x96\xa2 Disable wav error checking \xe2\x9c\x93' ;;
interactive|choose) local cddbOption='0' ; echo -e ' \xe2\x96\xa2 Interactive cddb selection \xe2\x9c\x93.' ;;
force) local force='yes' ; echo -e ' \xe2\x96\xa2 Force wav to opus encode \xe2\x9c\x93' ;;
nocddb|disablecddb) local cddbOption='-1' ; echo -e ' \xe2\x96\xa2 Disable CDDB enquiries \xe2\x9c\x93' ;;
deemphasize|filter) local filterPreEmphasis='-deemphasize' ; echo -e ' \xe2\x96\xa2 Filter pre-emphasis if found \xe2\x9c\x93' ;;
noclean) local noCleanExtractDirBeforeWrite='yes' ; noCleanProgress='yes' ; noCleanAfter='yes' ;;
nocleanbefore|cleanbeforeoff) local noCleanExtractDirBeforeWrite='yes' ; echo -e " \xe2\x96\xa2 $extractDir not cleaned \xe2\x9c\x93" ;;
nocleanafter|noclean|nocleanup) local noCleanAfter='yes' ; echo -e " \xe2\x96\xa2 Do not clean temporary directory $extractDir after wav creation \xe2\x9c\x93" ;;
nocleanprogress) local noCleanProgress='yes' ; echo -e ' \xe2\x96\xa2 Do not clean *.wav before next write \xe2\x9c\x93' ;;
sudo|sudoon) local sudoOn='sudo' ; echo -e ' \xe2\x96\xa2 Use sudo for cdrom access and *.wav writing \xe2\x9c\x93' ;;
flac) local fileFormat='flac' ;;
eject) local eject='yes' ;;
fast) local fast='yes' ;;
track) local track='yes' ;;
bitrate) local bitrate='yes' ;;
speed) local speed='yes' ;;
artist) local artist='yes' ;;
title) local title='yes' ;;
album) local album='yes' ;;
genre) local genre='yes' ;;
date) local date='yes' ;;
*) if [[ $track = 'yes' ]] ; then track=$option ; echo -e " \xe2\x96\xa2 Write track(s): \e[0;36m$option\e[0m \xe2\x9c\x93"
elif [[ $bitrate = 'yes' ]] ; then bitrate=$option ; echo -e " \xe2\x96\xa2 bitrate set at: \e[0;36m$option\e[0m \xe2\x9c\x93"
elif [[ $speed = 'yes' ]] ; then speed=$option ; echo -e " \xe2\x96\xa2 speed set at: \e[0;36m$option\e[0m \xe2\x9c\x93"
elif [[ $artist = 'yes' ]] ; then Albumperformer=$option ; echo -e " \xe2\x96\xa2 artist set at: \e[0;36m$option\e[0m \xe2\x9c\x93" ; unset artist
elif [[ $title = 'yes' ]] ; then Tracktitle=$option ; echo -e " \xe2\x96\xa2 title set at: \e[0;36m$option\e[0m \xe2\x9c\x93" ; unset title
elif [[ $album = 'yes' ]] ; then Albumtitle=$option ; echo -e " \xe2\x96\xa2 album set at: \e[0;36m$option\e[0m \xe2\x9c\x93" ; unset album
elif [[ $genre = 'yes' ]] ; then DGENRE=$option ; echo -e " \xe2\x96\xa2 genre set at: \e[0;36m$option\e[0m \xe2\x9c\x93" ; unset genre
elif [[ $date = 'yes' ]] ; then DYEAR=$option ; echo -e " \xe2\x96\xa2 date set at: \e[0;36m$option\e[0m \xe2\x9c\x93" ; unset date
fi ;; esac done

local speed=${speed:-'4'}
local bitrate=${bitrate:-'160'}
local cddbOption=${cddbOption:-'1'}
local fileFormat=${fileFormat:-'opus'}

[[ -z $noCleanExtractDirBeforeWrite ]] && $sudoOn rm -r $extractDir &> /dev/null
mkdir -p $extractDir
cd $extractDir

if [[ -n $track ]] ; then readarray -d ' ' trackList <<< "$track"
else readarray -t trackList <<< $($sudoOn cdda2wav dev=$cdromDevice -v audio-tracks)
trackList=("${trackList[@]//[[:space:]][[:digit:]]*/}") ; trackList=("${trackList[@]:1}")
fi

for number in "${trackList[@]//[[:space:]]/}"; do
zeroesToAdd=$((2 - ${#number}))
twoPlaces="${twoPlaces:0:$zeroesToAdd}"
number="${twoPlaces}${number}"

if [[ -n $fast && -z $writeOnce ]] ; then
$sudoOn cdda2wav dev=$cdromDevice -vall -no-hidden-track cddb=$cddbOption -B -Owav -P0 $filterPreEmphasis &> /dev/null && sdparm --quiet --command=stop $devDevice &&
writeOnce='done' && wavComplete+=("$number")
elif [[ -n $fast && -n $writeOnce ]] ; then wavComplete+=("$number")
else
[[ ! -e audio_${number}.wav ]] && { echo -e " \xe2\x96\xb6 Writing audio_${number}.wav." && $sudoOn cdda2wav dev=$cdromDevice -no-hidden-track -track $number cddb=$cddbOption -Owav -q -max -speed $speed $filterPreEmphasis $errorChecking audio_${number}.wav &> /dev/null ; }
wavComplete+=("$number")
fi

[[ $? = 0 && -e audio_${number}.cddb || -e audio.cddb || -n $force ]] && {
[[ $number = '01' || ${wavComplete[0]} != '01' && -z $Albumtitle ]] && {

if [[ -n $fast ]] ; then iconvConvert 'audio.cddb' else iconvConvert "audio_${number}.cddb" ; fi

if [[ -z $DYEAR ]] ; then search 'DYEAR' ; DYEAR="${output##*=}" ; fi
if [[ -z $DGENRE ]] ; then search 'DGENRE' ; DGENRE="${output##*=}" ; fi

iconvConvert "audio_${number}.inf"
if [[ -z $Albumtitle ]] ; then search 'Albumtitle' ; Albumtitle=${output#*\'} ; Albumtitle=${Albumtitle%\'} ; fi
mkdir -p "${workingDir}/${Albumtitle}"
}
[[ ${#wavComplete[@]} > '1' ]] && { [[ -z $noCleanProgress ]] && $sudoOn rm -r audio_"${wavComplete[-2]}".{wav,inf,cddb,cdindex,cdtext} &> /dev/null &
iconvConvert "audio_${number}.inf"
}
if [[ -z $Albumperformer ]] ; then search 'Albumperformer' ; Albumperformer=${output#*\'} ; Albumperformer=${Albumperformer%\'} ; fi
if [[ -z $Tracktitle ]] ; then search 'Tracktitle' ; Tracktitle=${output#*\'} ; Tracktitle=${Tracktitle%\'} ; Tracktitle=${Tracktitle//\//-} ; fi

if [[ $fileFormat = 'flac' ]] ; then echo -e " \xe2\x96\xb6 Encoding ${number} - \e[0;36m${Tracktitle}\e[0m.flac."
flac --compression-level-8 --silent -f "audio_${number}.wav" -o "${workingDir}/${Albumtitle}/${number} - ${Tracktitle}.flac" &
else echo -e " \xe2\x96\xb6 Encoding ${number} - \e[0;36m${Tracktitle}\e[0m.opus."
opusenc --quiet --bitrate="$bitrate" --artist="$Albumperformer" --title="$Tracktitle" --album="$Albumtitle" --genre="$DGENRE" --date="$DYEAR" "audio_${number}.wav" "${workingDir}/${Albumtitle}/${number} - ${Tracktitle}.opus" &
fi

if [[ ${trackList[-1]//[[:space:]]} != ${number#0} && -z $force ]] ; then unset Tracktitle ; fi ; } || { echo " ${extractDir}/*.cddb not found or cdda2wav error." ; return ; } ; done

sdparm --quiet --command=stop $devDevice &
[[ -z $noCleanAfter ]] && $sudoOn rm -r $extractDir &> /dev/null &
[[ -n $eject ]] && sdparm --quiet --command=eject $devDevice &
wait &> /dev/null
[[ -e "${workingDir}/${Albumtitle}/${number} - ${Tracktitle}.${fileFormat}" ]] && echo -e " \xe2\x98\xba *.${fileFormat} created \xe2\x96\xb6 ${workingDir}/\e[0;36m${Albumtitle}\e[0m \xe2\x97\x94 $SECONDS seconds elapsed."

set -m ; cd - &> /dev/null
}
