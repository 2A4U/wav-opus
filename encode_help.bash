#!/usr/bin/env bash

echo -e "
 \xe2\x96\xa2 track - example: '\e[0;36mencode track 1\e[0m' - create an opus file of a track.
 \xe2\x96\xa2 track - example 2: '\e[0;36mencode track \"1 3 5\"\e[0m' - multitrack selection note: use single or double quotes; use a space between numbers.
 \xe2\x96\xa2 fast - example: '\e[0;36mencode fast\e[0m' - write all *.wav files before *.opus encoding.
 \xe2\x96\xa2 artist - example: '\e[0;36mencode artist \"Words in Quotes\"\e[0m' - override CDDB info ; may use with 'force'.
 \xe2\x96\xa2 title - example: '\e[0;36mencode title \"Words in Quotes\"\e[0m' - override CDDB info ; may use with 'force'.
 \xe2\x96\xa2 album - example: '\e[0;36mencode album \"Words in Quotes\"\e[0m' - override CDDB info ; may use with 'force'.
 \xe2\x96\xa2 genre - example: '\e[0;36mencode genre soundtrack\e[0m' - override CDDB info ; may use with 'force'.
 \xe2\x96\xa2 date - example: '\e[0;36mencode date 2031\e[0m' - override CDDB info ; may use with 'force'.
 \xe2\x96\xa2 interactive|choose - example: '\e[0;36mencode interactive\e[0m' - user chooses from a cddb selection if available.
 \xe2\x96\xa2 nocddb|disablecddb - example: '\e[0;36mencode nocddb\e[0m' - disable CDDB query - do not connect to net.
 \xe2\x96\xa2 deemphasize|filter - example: '\e[0;36mencode filter\e[0m' - removes audio emphasis if found.
 \xe2\x96\xa2 nocheck|wavcheckoff - example: '\e[0;36mencode nocheck\e[0m' - do not check *.wav writing accuracy.
 \xe2\x96\xa2 nocleanbefore|cleanbeforeoff - example: '\e[0;36mencode nocleanbeforeoff\e[0m' - keep previously created temporary extract directory intact before new *.wav overwrite.
 \xe2\x96\xa2 nocleanprogress - example: '\e[0;36mencode nocleanprogress\e[0m' - do not clean before next write.
 \xe2\x96\xa2 nocleanafter|noclean|nocleanup - example: '\e[0;36mencode noclean\e[0m' - do not delete temporary extract directory after *.opus encode.
 \xe2\x96\xa2 bitrate - example: '\e[0;36mencode bitrate 256\e[0m' - writes *.opus files at the maximum bitrate of 256.
 \xe2\x96\xa2 speed - example: '\e[0;36mencode speed 24\e[0m' - writes *.wav at drive spin speed 24.
 \xe2\x96\xa2 eject - example: '\e[0;36mencode eject\e[0m' - eject removable disk after write.
 \xe2\x96\xa2 noclean - example: '\e[0;36mencode noclean\e[0m' - equal to nocleanbefore, nocleanprogress and nocleanafter.
 \xe2\x96\xa2 flac - example: '\e[0;36mencode flac\e[0m' - encode in FLAC format.
 \xe2\x96\xa2 force - example: '\e[0;36mencode noclean force track \"1 2 3\" title \"Custom Name\"\e[0m' - if database info is missing then force-write *.opus - use this to write custom opus info after *.wav creation - see also audio_01.inf.template.
"
