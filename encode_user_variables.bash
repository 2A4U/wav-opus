#!/usr/bin/env bash

#User variables (uncomment--remove '#' sign at beginning of line to use):

#workingDir="$HOME" #the default temporary directory is /dev/shm
#extractDir="${workingDir}/audio_CD" #default
#errorChecking='paranoia paraopts=minoverlap=30,readahead=1600,retries=400' #default
#cddbOption='1' #fast option ignores this variable # '0'=Interactive ; '1'=select the first cddb info ; '-1'=no cddb info downloaded #default is '1'
#speed=${speed:-'24'} #default speed is 4 without the 'fast' user option
#fast='yes' #writes all wav files at maximum drive speed before encoding to opus #default or empty variable is to encode immediately after wav creation
#bitrate=${bitrate:-'256} #default is '160'

